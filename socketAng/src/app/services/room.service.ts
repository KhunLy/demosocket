import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { Room } from '../models/room.model';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(
    private http: HttpClient,
    private socket: Socket
  ) { }

  getRooms(): Observable<Room[]> {
    return this.http.get<Room[]>('http://localhost:3000/rooms');
  }

  joinRoom(roomId: number) {
    this.socket.emit('joinRoom', roomId);
  }

  sendMessage(roomId, toSend: string) {
    this.socket.emit('newMessage', { room: roomId, message: toSend});
  }

  onRoomConnection() {
    return this.socket.fromEvent<string>('onConnection');
  }

  onRoomMessage() {
    return this.socket.fromEvent<string>('onMessage');
  }

  leaveRoom(roomId: number) {
    this.socket.emit('leaveRoom', roomId);
  }
}
