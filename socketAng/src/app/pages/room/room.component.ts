import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Room } from 'src/app/models/room.model';
import { RoomService } from 'src/app/services/room.service';

@Component({
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit, OnDestroy {
  
  private destroyed$: Subject<boolean>;

  roomId: number;

  roomMessage: string;

  toSend: string;

  usersMessages: string[] = [];

  constructor(
    private route: ActivatedRoute,
    private roomService: RoomService,
    //private ref: ChangeDetectorRef,
  ) {
    this.destroyed$ = new Subject<boolean>();
  }
 
  
  ngOnInit(): void {
    this.roomId = this.route.snapshot.params.id;

    this.roomService.joinRoom(this.roomId);
    
    this.roomService.onRoomConnection()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(message => {
        this.roomMessage = message;
      });
    
    this.roomService.onRoomMessage()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(message => {
        this.usersMessages.push(message);
        //this.ref.detectChanges();
      });
  }

  send() {
    this.roomService.sendMessage(this.roomId, this.toSend);
    this.toSend = null;
  }


  ngOnDestroy(): void {
    this.roomService.leaveRoom(this.roomId);
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}

