const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = require("socket.io")(server, {
    cors: {
      origin: "http://localhost:4200",
      methods: ["GET", "POST"]
    }
  });
const roomController = require('./controllers/room.controller');
const roomHub = require('./hubs/room.hub');

app.get('/rooms', roomController.getRooms);

io.on('connection', (socket) => {  
  socket.on('joinRoom', (room) => roomHub.joinRoom(socket, room));
  socket.on('newMessage', ({ room, message }) => roomHub.newMessage(socket, room, message));
  socket.on('leaveRoom', (room) => roomHub.leaveRoom(socket, room));
});

server.listen(3000, () => {
  console.log('listening on *:3000');
});