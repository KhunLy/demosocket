const fakeDb = {};

fakeDb.rooms = [
    { id: 1, name: 'Room 1' },
    { id: 2, name: 'Room 2' },
    { id: 3, name: 'Room 3' },
];

exports.getRooms = (req, res) => {
    res.json(fakeDb.rooms);
};