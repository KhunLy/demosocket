exports.joinRoom = (socket, room) => {
    // join a specific group
    socket.join(room);

    socket.emit('onConnection', `Welcome to room ${room}`);

    // send to everybody in the group except the caller
    socket.to(room).emit('onConnection', `Socket ${socket.id} has join the room`);
}

exports.newMessage = (socket, room, message) => {
    socket.to(room).emit('onMessage', message);
}

exports.leaveRoom = (socket, room) => {
    socket.leave(room);

    socket.to(room).emit('onConnection', `Socket ${socket.id} has leave the room`);
}